package com.jackren.example.springboot.core.database;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.alibaba.druid.pool.DruidDataSource;
import com.jackren.example.springboot.core.constant.ProjectConstant;

import tk.mybatis.spring.annotation.MapperScan;

@Configuration
@MapperScan(basePackages = {
		ProjectConstant.BASE_PACKAGE + ".dao.db2" }, sqlSessionFactoryRef = "secondSqlSessionFactory")
public class MysqlDataSource2Config {

	@Bean(name = "secondDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.db2")
	public DataSource dataSource() {
		return new DruidDataSource();
	}

	@Bean(name = "secondTransactionManager")
    public DataSourceTransactionManager secondTransactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }
	
	@Bean(name = "secondSqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		Resource[] mapperLocations = new PathMatchingResourcePatternResolver()
				.getResources("classpath:mapper/db2/*.xml");
		sessionFactory.setMapperLocations(mapperLocations);
		return sessionFactory.getObject();
	}
}
