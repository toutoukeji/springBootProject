package com.jackren.example.springboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jackren.example.springboot.restful.RetResponse;
import com.jackren.example.springboot.restful.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/")
@Api(tags = {"首页访问"}, description = "indexController")
public class IndexController {
	
	@GetMapping("")
	@ApiOperation(value = "首页入口", notes = "get 请求进入首页")
	public RetResult<String> index() {
		return RetResponse.makeOKRsp();
	}
}
